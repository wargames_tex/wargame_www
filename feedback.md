# Feedback 

Comments, questions, suggestions, correction, critique and so on is
most welcome.   Please use 

* The _Issue_ feature on the GitLab page of the
  [game](games.html){target=_self} or [tool](tools.html){target=_self}
  in question,
* The [file-upload](upload.html){target=_self} thread on BoardGameGeek
  of the game in question, or
* The [module](modules.html){target=_self} posting at the VASSAL
  library of the game in question.

I would greatly appreciate if you will leave a like in the
BoardGameGeek [file-upload](uploads.html){target=_self} thread or with
[module](vassal.html){target=_self} posting  in the VASSAL library. 

If you have questions about rules in the games, the best place to post
them is at BoardGameGeek.  Since rewriting the rules of a game often
entail gaining a fairly deep understand of the rules, I will often be
in the position to answer questions.  Or I will try at least. 

