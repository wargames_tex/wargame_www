# Build the documents and modules

- [Introduction](#introduction){target=_self}
- [Prerequisites](#prerequisites){target=_self}
  - [Debian GNU/Linux and derivatives](#debian-gnulinux-and-derivatives){target=_self}
  - [RedHat and derivatives](#redhat-and-derivatives){target=_self}
  - [MacOSX](#macosx){target=_self}
  - [Windows](#windows){target=_self}
- [Get the LaTeX wargame package](#get-the-wargame-package){target=_self}
  - [From CTAN](#from-ctan){target=_self}
    - [Un*x-like](#unx-like){target=_self}
    - [Windows](#windows-1){target=_self}
  - [From GitLab](#from-gitlab){target=_self}
    - [`make` on Un*x-like platforms](#make-on-unx-like-platforms){target=_self}
    - [By-hand for all platforms](#by-hand-for-all-platforms){target=_self}
      - [Install by-hand on Un*x-like platforms](#install-by-hand-on-unx-like-platforms){target=_self}
      - [Install by-hand on Windows](#install-by-hand-on-windows){target=_self}
- [Building the Print’n’Play PDF](#building-the-printnplay-pdf){target=_self}
  - [Obtain the sources](#obtain-the-sources){target=_self}
    - [Get a source archive](#get-a-source-archive){target=_self}
    - [Clone from GitLab](#clone-from-gitlab){target=_self}
  - [`make` PDF on Un*x-like platforms](#make-pdf-on-unx-like-platforms){target=_self}
  - [Build the PDF by-hand on all platforms](#build-the-pdf-by-hand-on-all-platforms-1){target=_self}
  
--- 

## Introduction 

The information provided here is for people that want to dive deeper
into the sources of the games themselves, or want to change some
aspect in the games.    It is therefore a little longer and more
detailed than what most users need - or care to - read. 

In other words, unless you are diving into the games and how they are
authored, perhaps because you want to make your own, then you should
probably stop reading now and head over to the
[game](games.html){target=_self} or
[module](vassal.html){target=_self} listing.
  
---

## Prerequisites

To build the documents, you need a $\text{\TeX}$ and friends
installation.  If you also want to build the [VASSAL][vassal] modules,
you also need an installation of [Python][python], the
[Pillow][pillow] Python module, and [Poppler][poppler].  If you want
to build the [LaTeX Wargames][wargames_www] directly from
[GitLab][wargames_git], then you will also need a [Git][git] client.

### Debian GNU/Linux and derivatives 

To install all the prerequisites on a Debian GNU/Linux, or any of its
derivatives like Ubuntu, do 

    sudo apt install texlive-pictures python3-pil poppler-utils
    
To get a [Git][git] client do 

    sudo apt install git 
    
### RedHat and derivatives 

To install all the prerequisites on a RedHat GNU/Linux, or any of its
derivatives like Fedora and CentOS, do 

    sudo yum install texlive python3-pillow poppler-utils
 
To get a [Git][git] client do 

    sudo yum install git 
    
### MacOSX 

To install $\text{\TeX}$ and friends get 

- [MacTeX][mactex]

and install as per usual MacOSX software.  Next you need to get 

- [Python][python_mac] 

and, again, install per the usual MacOSX installation procedure.  You
also need the Python [Pillow][pillow] module.  To install that, fire
up a [Terminal][terminal_mac] and do

    pip install pillow
    
MacTeX comes with editor and viewer [TeXShop][texshop] for editing and
running $\text{\LaTeX}$.

To get a [Git][git] client visit 

- [Git for MacOSX](git_mac)

and download the installer and install as any other MacOSX
application. 

### Windows 

To install $\text{\TeX}$ and friends get 

- [TeXLive][texlive_win]

and install as per usual Windows software.  Next you need to get 

- [Python][python_win]

and install that as any other Windows application.  Be sure to check
the option to _Add `python.exe` to `PATH`_.  You need the Python
[Pillow][pillow] module.  To install that, [open a
terminal][terminal_win] and do

    pip install pillow 
    
TeXLive comes with the editor and viewer [TeXworks][texworks] for
editing and running $\text{\LaTeX}$.

To get a [Git][git] client visit 

- [Git for Windows](git_win)

and download the installer and install as any other Windows
application. 

---

## Get the wargame package 

See also the [`wargame` GitLab page][wargame_tex] for more
information. 

The `wargame` package is part of [TeXLive][texlive] and
[MacTeX][mactex] as
[`/macros/latex/contrib/wargame`][wargame_contrib], and you may not
need to do more.

However, if you need the latest and greatest(?) version of the
package, you can install it parallel to the TeXLive or MacTeX
installation.  You can either install from [CTAN][wargame_ctan] or
[GitLab][wargame_tex].

### From CTAN 

For all platforms, go to the [`wargame` CTAN page](wargame_ctan) and
download 

- [wargame.tds.zip][wargame_tds] ZIP archive 

If you'd rather build the package yourself, then download _all_ the
files _and_ folders from the
[/macros/latex/contrib/wargame/source/][wargame_src] directory and
follow the instructions [below for a GitLab build
installation](#by-hand-for-all-platforms).  Be sure to keep the
directory structure. 

You can install that either in a system wide directory or for a
specific user. 

#### Un*x-like 

For Un*x-like platforms, e.g., GNU/Linux or MacOSX, the system-wide
local $\text{\TeX}$ and friends directory is 

    /usr/local/share/texmf 
    
while the user-specific directory is 

    ~/texmf 
    
To install the `wargame` package system-wide, open a terminal and do 

    sudo mkdir -p /usr/local/share/texmf 
    sudo unzip wargame.tds.zip -d /usr/local/share/texmf 
    sudo texhash 
    
To install `wargame` for a single user, do 

    mkdir -p ~/texmf 
    unzip wargame.tds.zip -d ~/texmf 
    
#### Windows 

Assuming TeXLive was installed in `C:\texlive`, then the system-wide
local installation directory is 

    C:\texlive\texmf-local 
    
The user specific $\text{\TeX}$ and friends directory is 

    %USERPROFILE%\texmf 
    
where `%USERPROFILE%` is typically `C:\Users\user_name`, and
`user_name` is your Windows user name.  To install in the system-wide
local directory [open a terminal][terminal_win] and so 

    unzip %USERPROFILE%\Downloads\wargame.tds.zip -d C:\texlive\texmf-local
    texhash 
    
To install for a specific user, again, [open a terminal][terminal_win]
and do 

    mkdir %USERPROFILE%\texmf
    unzip %USERPROFILE%\Downloads\wargame.tds.zip -d %USERPROFILE%\texmf

### From GitLab 

First, you need to clone the [GitLab][wargame_tex] repository.  Open a
terminal and do 

    git clone https://gitlab.com/wargames_tex/wargame_tex.git
    
Then, change directory into the newly created `wargame_tex` directory 

    cd wargame_tex 
    
#### `make` on Un*x-like platforms 

If you are on a Un*x-like platform, e.g., GNU/Linux or MacOSX, you can
use the [`make`][make] utility to build and install the package.  As
for the [CTAN](#from_ctan) installation above, you can chose between
installing system-wide or for a single user.

To install system-wide, do 

    sudo make install DESTDIR=/usr/local/share/texmf
    
To install for a single user, do 

    make install 
    
If you prefer to build the package "by-hand", or you do not have
`make` installed, follow the instructions below 

#### By-hand for all platforms

In the directory with all the source files (the directory with the
file `package.dtx`), do 

    latex wargame.ins
    pdflatex wargame.beach
    pdflatex wargame.city
    pdflatex wargame.light_woods
    pdflatex wargame.mountains
    pdflatex wargame.rough
    pdflatex wargame.swamp
    pdflatex wargame.town
    pdflatex wargame.village
    pdflatex wargame.woods

This generates the package.  If you also want to make the
documentation, do 

    pdflatex wargame.dtx
    makeindex -s gind -o wargame.ind wargame.idx
    pdflatex wargame.dtx
    pdflatex wargame.dtx
    pdflatex symbols.tex
    pdflatex compat.tex
    pdflatex compat.tex

Then copy the files to your installation directory.  Again, where you
copy depends on whether you want to install system-wide or for a
single user.   How you install also depends on your platform. 

##### Install by-hand on Un*x-like platforms 

For Un*x-like platforms, e.g., GNU/Linux or MacOSX, the system-wide
local installation directory is 

    /usr/local/share/texmf 
    
while the user specific installation directory is 

    ~/texmf 
    
To install in to the system-wide directory, do 

    mkdir /usr/local/share/texmf/tex/latex/wargame
    cp tikzlibrary*.tex /usr/local/share/texmf/tex/latex/wargame/
    cp wargame.sty      /usr/local/share/texmf/tex/latex/wargame/
    cp wgexport.cls     /usr/local/share/texmf/tex/latex/wargame/
    cp wargame.*.pdf    /usr/local/share/texmf/tex/latex/wargame/
    cp wgexport.py      /usr/local/share/texmf/tex/latex/wargame/
    cp wgsvg2tikz.py    /usr/local/share/texmf/tex/latex/wargame/

To also install the documentation do 

    mkdir -p /usr/local/share/texmf/doc/latex/wargame/
    cp wargame.pdf   /usr/local/share/texmf/doc/latex/wargame/
    cp symbols.pdf   /usr/local/share/texmf/doc/latex/wargame/
    cp compat.pdf    /usr/local/share/texmf/doc/latex/wargame/
 
 
To install in the user specific directory, do 

    mkdir ~/texmf/tex/latex/wargame
    cp tikzlibrary*.tex ~/texmf/tex/latex/wargame/
    cp wargame.sty      ~/texmf/tex/latex/wargame/
    cp wgexport.cls     ~/texmf/tex/latex/wargame/
    cp wargame.*.pdf    ~/texmf/tex/latex/wargame/
    cp wgexport.py      ~/texmf/tex/latex/wargame/
    cp wgsvg2tikz.py    ~/texmf/tex/latex/wargame/

To also install the documentation to the same user directory, do 

    mkdir -p ~/texmf/doc/latex/wargame/
    cp wargame.pdf   ~/texmf/doc/latex/wargame/
    cp symbols.pdf   ~/texmf/doc/latex/wargame/
    cp compat.pdf    ~/texmf/doc/latex/wargame/

##### Install by-hand on Windows

Assuming TeXLive was installed in `C:\texlive`, then the system-wide
local installation directory is 

    C:\texlive\texmf-local 
    
The user specific $\text{\TeX}$ and friends directory is 

    %USERPROFILE%\texmf 
    
where `%USERPROFILE%` is typically `C:\Users\user_name`, and
`user_name` is your Windows user name.  To install in the system-wide
local directory, do

    cp tikzlibrary*.tex c:\texlive\texmf-local\tex\latex\wargame\
    cp wargame.sty      c:\texlive\texmf-local\tex\latex\wargame\
    cp wgexport.cls     c:\texlive\texmf-local\tex\latex\wargame\
    cp wargame.*.pdf    c:\texlive\texmf-local\tex\latex\wargame\
    cp wgexport.py      c:\texlive\texmf-local\tex\latex\wargame\
    cp wgsvg2tikz.py    c:\texlive\texmf-local\tex\latex\wargame\

To also install the documentation to the same user directory, do 

    cp wargame.pdf   c:\texlive\texmf-local\doc\latex\wargame\
    cp symbols.pdf   c:\texlive\texmf-local\doc\latex\wargame\
    cp compat.pdf    c:\texlive\texmf-local\doc\latex\wargame\


To install in a user specific directory, do 

    mkdir %USERPROFILE%\texmf\tex\latex\wargame
    cp tikzlibrary*.tex %USERPROFILE%\texmf\tex\latex\wargame\
    cp wargame.sty      %USERPROFILE%\texmf\tex\latex\wargame\
    cp wgexport.cls     %USERPROFILE%\texmf\tex\latex\wargame\
    cp wargame.*.pdf    %USERPROFILE%\texmf\tex\latex\wargame\
    cp wgexport.py      %USERPROFILE%\texmf\tex\latex\wargame\
    cp wgsvg2tikz.py    %USERPROFILE%\texmf\tex\latex\wargame\

To also install the documentation to the same user directory, do 

    mkdir %USERPROFILE%\texmf\doc\latex\wargame
    cp wargame.pdf   %USERPROFILE%\texmf\doc\latex\wargame\
    cp symbols.pdf   %USERPROFILE%\texmf\doc\latex\wargame\
    cp compat.pdf    %USERPROFILE%\texmf\doc\latex\wargame\

---

## Building the Print'n'Play PDF

First off, you should decide which game you want to build.  Here, we
will use the game [_Battle for Moscow_](games.html#battle-for-moscow)
as an example.

For other games available at [GitLab][wargames_git], make the
appropriate substitutions of filenames etc. below.  For example, for
[_Afrika Korps_][ak_tex], the main $\text{\LaTeX}$ source file name is
`AfrikaKorps.tex`, and so on. 

### Obtain the sources 

The next thing to do is to obtain the sources.   You can do that in
two ways - either by downloading the sources as an archive, or by
doing a clone from GitLab.  

#### Get a source archive 

For _Battle for Moscow_, you can head over to the [GitLab
repository][bfm_git].  There, you will a blue button on the top right
that says **Code**.   When you click that, you get a menu with the section
_Download the source code_.  Under that, you will have a number of
options. Here, we will choose the `zip` option.  This will then
download the sources as a ZIP archive - which for _Battle for Moscow_
will be called `bfm_tex-master.zip`. 

Next, open up a terminal and go to where you want to work on this.
Move the just downloaded ZIP archive there and unpack with 

    unzip bfm_tex-master 
    
This will create the directory `bfm_tex-master`.  We will rename that
to `bfm_tex` for consistency with a GitLab clone.   On Uni*x like
platforms, do 

    mv bfm_tex-master bfm_tex 
    
On Windows, do 

    rename bfm_tex-master bfm_tex 
    
Now change directory to the source directory 

    cd bfm_tex 
    
#### Clone from GitLab 

For _Battle for Moscow_, you can head over to the [GitLab
repository][bfm_git].  There, you will a blue button on the top right
that says **Code**.   When you click that, you get a menu with the
option _Clone with HTTPS_ and under that a URL.  Either copy the URL
or press the clip-board icon right of it.

Open up a terminal and go where you want to work on _Battle for
Moscow_.  Then do 

    git https://gitlab.com/wargames_tex/bfm_tex.git
    
where the `https://...` is what you copied above (paste with Ctrl-V or
similar). 

This will create the directory `bfm_tex`. Now change directory to the
source directory

    cd bfm_tex 
    
### `make` PDF on Un*x-like platforms 

On Un*x-like platforms, e.g., GNU/Linux or MacOSX, you can use the
[`make`][make] tool to create the Print'n'Play PDF.   Simply do 

    make a4 
    
for A4-printers in `BattleForMoscowA4.pdf`, and 

    make letter 
    
for Letter printers in `BattleForMoscowLetter.pdf`. 

If you prefer to build by-hand, see the next section. 

### Build the PDF by-hand on all platforms

The master file for _Battle for Moscow_ is called
`BatteForMoscow.tex`.   We need to run $\text{\LaTeX}$ in that file
twice.   To do that, do 

    pdflatex BattleForMoscow.tex
    pdflatex BattleForMoscow.tex

Alternatively, you can open the file `BattleForMoscow.tex` in your
favourite $\text{\TeX}$ and friends editor (e.g., [TeXShop][texshop],
[TeXworks][texworks], Emacs, Vi, or something else) and use that
editors interface to run $\text{\LaTeX}$. 

In any case, you should now have the file `BattleForMoscow.pdf`. 

Note that PDF is by default for A4 printers.  If you need a PDF
formatted for Letter, you should do 


    pdflatex -jobname BattleForMoscowLetter BattleForMoscow.tex
    pdflatex -jobname BattleForMoscowLetter BattleForMoscow.tex
    
    
If you need a separate PDF of only the materials, you can do 

    pdflatex materials.tex
    pdflatex materials.tex

Again, this is designed for A4 printers.  To make it for Letter
printers, do 

    pdflatex -jobname materialsLetter materials.tex
    pdflatex -jobname materialsLetter materials.tex

## Build VASSAL module 

As mentioned [above](#prerequisites) you must have Python and some
other packages installed.  I will also assume that you already [got
the sources](#obtain-the-sources), and I will continue to use _Battle
for Moscow_ as the example, and that we have terminal opened in
`bfm_tex`. 

### `make` VMOD on Un*x-like platforms 

On Un*x-like platforms, e.g., GNU/Linux or MacOSX, you can use the
[`make`][make] tool to create the [VASSAL][vassal] module.   Simply do 

    make vmod
    
to get `BattleForMoscowA4.vmod`.

### Build the VMOD by-hand on all platforms

We will process the special `export.tex` document with
$\text{\LaTeX}$.  This file defines all the images that we will use in
the VASSAL module. 

    pdflatex export.tex 
    
Note that this may take a little while.  This process generates
`export.pdf` and `export.json`.  The latter contain some
meta-information for all the images in `export.pdf`.  To create our
VASSAL module, we will assume that you have already build
`BattleForMoscowA4.pdf` above, so that we can attach that to the
module as the rules.  

Now we need to execute the Python script `wgexport.py`.  This is part
of the `wargame` package and need not be installed separately.
However, the script is most likely not in your path, so we need to
figure where it is.  Fortunately, that is dead-easy - just do 

    kpsewhich wgexport.py 
    
This will report the path to the script.  Let us assume a Un*x-like
system and that the script is in
`/usr/share/texlive/texmf-dist/tex/latex/wargame/wgexport.py`. 

When we execute this script, we can pass another Python script to it,
which will then get called on the build VASSAL module.  In _Battle for
Moscow_, we have the script `patch.py` for that.  This will set up all
sorts of things for the module.   

To generate the module, do 

    /usr/share/texlive/texmf-dist/tex/latex/wargame/wgexport.py \
      export.pdf export.json \
      -o BattleForMoscow.vmod \
      -t "Battle for Moscow" \
      -v 1.2 \
      -p patch.py
      -r BattleForMoscowA4.pdf \
      -T Tutorial.vlog \
      -d "This module was created from LaTeX sources" 


Running the above command generates `BattleForMoscow.vmod` which is
our final VASSAL module. 

Above, `\` are line-continuation markers.  Some systems may not
support that, in which case all lines should be put on one line
(without the `\` separators).   To more on the various options passed,
do 

    /usr/share/texlive/texmf-dist/tex/latex/wargame/wgexport.py --help
    
Also note, that on Windows the above command will open a new terminal
which will quickly close again.  To execute the script in the
_current_ terminal, do 

    python 
      /usr/share/texlive/texmf-dist/tex/latex/wargame/wgexport.py \
        export.pdf export.json \
        -o BattleForMoscow.vmod \
        -t "Battle for Moscow" \
        -v 1.2 \
        -p patch.py
        -r BattleForMoscowA4.pdf \
        -T Tutorial.vlog \
        -d "This module was created from LaTeX sources" 


[wargames_www]: https://wargames_tex.gitlab.io/wargame_www/
[wargames_git]: https://gitlab.com/wargames_tex
[wargame_tex]: https://gitlab.com/wargames_tex/wargame_tex
[wargame_ctan]: https://ctan.org/pkg/wargame
[wargame_contrib]: https://ctan.org/tex-archive/macros/latex/contrib/wargame
[wargame_src]: https://ctan.org/tex-archive/macros/latex/contrib/wargame/source
[wargame_tds]: https://mirrors.ctan.org/install/macros/latex/contrib/wargame.tds.zip
[texlive]: https://www.tug.org/texlive/.html
[texlive_win]: https://www.tug.org/texlive/windows.html
[mactex]: https://www.tug.org/mactex/
[python]: https://python.org
[python_win]: https://www.python.org/downloads/windows/
[python_mac]: https://www.python.org/downloads/macos/
[git]: https://git-scm.com/
[git_mac]: https://git-scm.com/download/mac
[git_win]: https://git-scm.com/download/win
[pillow]: https://pillow.readthedocs.io/en/stable/
[poppler]: https://poppler.freedesktop.org/
[terminal_win]: https://learn.microsoft.com/en-us/windows/terminal/faq#how-do-i-run-a-shell-in-windows-terminal-in-administrator-mode
[terminal_mac]: https://support.apple.com/guide/terminal/open-or-quit-terminal-apd5265185d-f365-44cb-8b09-71a064a42125/mac
[texshop]: https://pages.uoregon.edu/koch/texshop/
[texworks]: https://www.tug.org/texworks/
[vassal]: https://vassalengine.org
[make]: https://www.gnu.org/software/make/
[bfm_git]: https://gitlab.com/wargames_tex/bfm_tex 
[ak_git]: https://gitlab.com/wargames_tex/ak_tex 
