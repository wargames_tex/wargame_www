# BoardGameGeek uploads 

Below are links to each games uploaded file page on
[BoardGameGeek](https://boardgamegeek.com) (BGG). BGG has a [summary
page](https://boardgamegeek.com/files/boardgame/all?username=cholmcc)
of my uploads.

The PDFs can also be found through each [game's GitLab
page](games.html){target=_self}.  The PDFs found at GitLab is always
the newest possible.

### [![](https://gitlab.com/wargames_tex/afrikakorps_tex/-/raw/master/.imgs/front.png?ref_type=heads) Afrika Korps](https://boardgamegeek.com/filepage/248857)

### [![](https://gitlab.com/wargames_tex/battle_tex/-/raw/master/.imgs/front.png?ref_type=heads) Battle](https://boardgamegeek.com/filepage/256460)

### [![](https://gitlab.com/wargames_tex/bfm_tex/-/raw/master/.imgs/front.png?ref_type=heads) Battle for Moscow](https://boardgamegeek.com/filepage/244047)

### [![](https://gitlab.com/wargames_tex/sbotb_tex/-/raw/master/.imgs/front.png?ref_type=heads) Battle of the Bulge (Smithsonian)](https://boardgamegeek.com/filepage/260420)

### [![](https://gitlab.com/wargames_tex/dday_tex/-/raw/master/.imgs/front.png?ref_type=heads) D-Day 3rd Ed.](https://boardgamegeek.com/filepage/247710)

### [![](https://gitlab.com/wargames_tex/sdday_tex/-/raw/master/.imgs/front.png?ref_type=heads) D-Day (Smithsonian)](https://boardgamegeek.com/filepage/248721)

### [![](https://gitlab.com/wargames_tex/firstblood_tex/-/raw/master/.imgs/front.png?ref_type=heads) First Blood](https://boardgamegeek.com/filepage/244670)

### [![](https://gitlab.com/wargames_tex/sgb_tex/-/raw/master/.imgs/front.png?ref_type=heads) Gettysburg (Smithsonian)](https://boardgamegeek.com/filepage/262912)

### [![](https://gitlab.com/wargames_tex/kriegspiel_tex/-/raw/master/.imgs/front.png?ref_type=heads) Kriegspiel](https://boardgamegeek.com/filepage/242550)

### [![](https://gitlab.com/wargames_tex/naw_tex/-/raw/master/.imgs/front.png) Napleon at Waterloo](https://boardgamegeek.com/filepage/278514)

### [![](https://gitlab.com/wargames_tex/outdoor_tex/-/raw/master/.imgs/front.png?ref_type=heads) Outdoor Survival](https://boardgamegeek.com/filepage/259394)

### [![](https://gitlab.com/wargames_tex/portstanley_tex/-/raw/master/.imgs/frontA4.png?ref_type=heads) Port Stanley](https://boardgamegeek.com/filepage/253357)

### [![](https://gitlab.com/wargames_tex/paa_tex/-/raw/master/.imgs/front.png?ref_type=heads) Panzerarmee Afrika](https://boardgamegeek.com/filepage/)

### [![](https://gitlab.com/wargames_tex/sfo_tex/-/raw/main/.imgs/front.png?ref_type=heads) Strike Force One](https://boardgamegeek.com/filepage/249198)

### [![](https://gitlab.com/wargames_tex/dom_tex/-/raw/master/.imgs/front.png?ref_type=heads) The Drive on Metz](https://boardgamegeek.com/filepage/262887)

