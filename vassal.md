# VASSAL modules

* [Generally](#generally){target=_self}
* [Modules](#modules){target=_self}
  * [Afrika Korps](#afrika-korps){target=_self}
  * [Battle](#battle){target=_self}
  * [Battle for Moscow](#battle-for-moscow){target=_self}
  * [Battle of the Bulge](#battle-of-the-bulge){target=_self}
  * [D-Day (3rd Ed.)](#d-day-3rd-ed.){target=_self}
  * [D-Day (Smithsonian)](#d-day-smithsonian){target=_self}
  * [First Blood](#first-blood){target=_self}
  * [Gettysburg (Smithsonian)](#gettysburg-smithsonian){target=_self}
  * [Napoleon at Waterloo](#napoleon-at-waterloo){target=_self}
  * [Outdoor Survival](#outdoor-survival){target=_self}
  * [Panzerarmee Afrika](#panzerarmee-afrika){target=_self}
  * [Port Stanley](#port-stanley){target=_self}
  * [Strike Force One](#strike-force-one){target=_self}
  * [The Drive on Metz](#the-drive-on-metz){target=_self}

## Generally 

Many of the [games](games.html) are also available as 
[VASSAL](https://vassalengine.org) modules.  These modules generally
features 

- Contain the rules in the **Help** menu
- Battle markers 
- Automatic combat "odds" calculations 
- Automatic combat resolution 
- Automatic victory point calculations 
- Tutorial 

The modules are made from the L<span class="latex-a">A</span>T<span
class="latex-e">E</span>X sources, via the package
[`wargame`](wargame.html) and a Python patch script that uses the
[`pywargame`](pywargame.html) API. 

## Modules 

Below are links to each games [VASSAL](https://vassalengine.org)
module page.  The modules can also be found through each [game's
GitLab page](games.html){target=_self}.  The module found at GitLab is
always the newest possible.

### [Afrika Korps](https://vassalengine.org/wiki/Module:Afrika_Korps)

![](https://gitlab.com/wargames_tex/afrikakorps_tex/-/raw/master/.imgs/vassal.png?ref_type=heads){class=screenshot}

### [Battle](https://vassalengine.org/wiki/Module:Battle:_The_Game_of_Generals)

![](https://gitlab.com/wargames_tex/battle_tex/-/raw/master/.imgs/vassal.png?ref_type=heads){class=screenshot}

### [Battle for Moscow](https://vassalengine.org/wiki/Module:Battle_for_Moscow)

![](https://gitlab.com/wargames_tex/bfm_tex/-/raw/master/.imgs/vassal.png?ref_type=heads){class=screenshot}

### [Battle of the Bulge (Smithsonian)](https://vassalengine.org/wiki/Module:Battle_of_the_Bulge_-_Smithsonian_Edition)

![](https://gitlab.com/wargames_tex/sbotb_tex/-/raw/master/.imgs/vassal.png?ref_type=heads){class=screenshot}

### [D-Day (3rd Ed.)](https://vassalengine.org/wiki/Module:D-Day)

![](https://gitlab.com/wargames_tex/dday_tex/-/raw/master/.imgs/vassal.png?ref_type=heads){class=screenshot}

### [D-Day (Smithsonian)](https://vassalengine.org/wiki/Module:D-Day_-_Smithsonian_Edition)

![](https://gitlab.com/wargames_tex/sdday_tex/-/raw/master/.imgs/vassal.png?ref_type=heads){class=screenshot}

### [First Blood](https://vassalengine.org/wiki/Module:First_Blood:_The_Guadalcanal_Campaign)

![](https://gitlab.com/wargames_tex/firstblood_tex/-/raw/master/.imgs/vassal.png?ref_type=heads){class=screenshot}

### [Gettysburg (Smithsonian)](https://vassalengine.org/wiki/Module:Gettysburg:_125th_Anniversary_Edition)

![](https://gitlab.com/wargames_tex/sgb_tex/-/raw/master/.imgs/vassal.png?ref_type=heads){class=screenshot}

### [Napoleon at Waterloo](https://vassalengine.org/wiki/Module:Napoleon_at_Waterloo)

![](https://gitlab.com/wargames_tex/naw_tex/-/raw/master/.imgs/vassal.png){class=screenshot}

### [Outdoor Survival](https://vassalengine.org/wiki/Module:Outdoor_Survival)

![](https://gitlab.com/wargames_tex/outdoor_tex/-/raw/master/.imgs/vassal.png?ref_type=heads){class=screenshot}

### [Panzerarmee Afrika](https://vassalengine.org/wiki/Module:PanzerArmee_Afrika:_Rommel_in_the_Desert,_April_1941_-_November_1942)

![](https://gitlab.com/wargames_tex/paa_tex/-/raw/master/.imgs/vassal.png?ref_type=heads){class=screenshot}

### [Port Stanley](https://vassalengine.org/wiki/Module:Port_Stanley:_Battle_for_the_Falklands)

![](https://gitlab.com/wargames_tex/portstanley_tex/-/raw/master/.imgs/vassal.png?ref_type=heads){class=screenshot}

### [Strike Force One](https://vassalengine.org/wiki/Module:Strike_Force_One:_The_Cold_War_Heats_Up_%E2%80%93_1975)

![](https://gitlab.com/wargames_tex/sfo_tex/-/raw/main/.imgs/vassal.png?ref_type=heads){class=screenshot}

### [The Drive on Metz](https://vassalengine.org/wiki/Module:The_Drive_on_Metz,_1944)

![](https://gitlab.com/wargames_tex/dom_tex/-/raw/master/.imgs/vassal.png?ref_type=heads){class=screenshot}



