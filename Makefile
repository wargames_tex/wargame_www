PANDOC		:= pandoc
PANDOC_FLAGS	:= --webtex
LATEX		:= pdflatex
LATEX_FLAGS	:=
PDFTOCAIRO	:= pdftocairo
PDFTOCAIRO_FLAGS:= -png -scale-to 800 -singlefile

SKELS		:= top-skel.html sidebar-skel.html content-skel.html
MDS		:= copyright.md		\
		   feedback.md		\
		   games.md		\
		   howto.md		\
		   build.md		\
		   index.md		\
		   more.md		\
		   sidebar.md		\
		   sources.md		\
		   tools.md		\
		   uploads.md		\
		   vassal.md
PNGS		:= board2x4.png		\
		   board2x7.png		\
		   board3x3.png		\
		   board4x3.png		\
		   counters.png		\
		   countersglue.png	\
		   countersalt.png	\
		   gamestack.png	\
		   pywargame.png	\
		   wargame.png		
TARGETS		:= $(patsubst %.md, %.html, $(filter-out sidebar.md, $(MDS))) \
		   $(PNGS)		\
		   style.css		
MENUS		:= games		\
		   tools		\
		   more

%-content.html:%.md
	@echo "Content of $*"
	@$(PANDOC) $(PANDOC_FLAGS) -o $@ $<

%.html:%-content.html sidebar-filled.html $(SKELS)
	@echo "Page of $*"
	@sed 's/@title@/\u$*/' < top-skel.html > $@
	@sed -e 's/href="$@"/class="current" href="$@"/' \
		< sidebar-filled.html >> $@
	@sed -e '/@sidebar@/d' < sidebar-skel.html >> $@
	@cat $< >> $@ 
	@sed -e '/@content@/d' < content-skel.html >> $@

%.pdf:%.tex
	$(LATEX) $(LATEX_FLAGS) $<

%.png:%.pdf
	$(PDFTOCAIRO) $(PDFTOCAIRO_FLAGS) $< 

all:	$(TARGETS)

show:
	@$(foreach t, $(TARGETS), echo "$(t)";)

sidebar-filled.html: sidebar-content.html 	\
		     $(MENUS:%=%-content.html)
	@echo "Build navigation"
	@cp $< $<.tmp
	@for menu in $(MENUS); do 				  \
	  u=`echo "$$menu" | tr "[a-z]" "[A-Z]"` 		; \
	  csplit -s $<.tmp "/$$u/" 				; \
	  cat xx00                              		>  $<.tmpp ; \
	  head -n 1 xx01 | sed "s/$$u.*/<div class=sub>/" 	>> $<.tmpp ; \
	  cat $${menu}-content.html				>> $<.tmpp ; \
	  echo "</div></li>"					>> $<.tmpp ; \
	  tail -n +2 xx01					>> $<.tmpp ; \
	  mv $<.tmpp $<.tmp 					; \
	done
	@rm -f xx*
	@cat $<.tmp | sed 's/id="/id="sb-/g' > $@
	#@mv $<.tmp $@

$(SKELS):	skel.thtml
	@echo "Build skeleton"
	@csplit -s $< '/@sidebar@/' '/@content@/'
	@mv xx00 top-skel.html
	@mv xx01 sidebar-skel.html
	@mv xx02 content-skel.html

clean:
	rm -f *.html xx* *~
	rm -f *.log *.aux *.synctex* *.pdf
	rm -rf public 

realclean:clean
	rm -f $(IMAGES)

docker:
	docker run --user root --group-add users -e GRANT_SUDO=yes -it --rm \
		-v $(PWD):/root/$(notdir $(PWD)) texlive/texlive \
		/bin/bash


docker-prep:
	apt update
	apt install -y pandoc poppler-utils


dist:	$(TARGETS)
	mkdir -p public
	cp $(TARGETS) public/

#
# EOF
#
