# Some comments on copyright 

_Caveat_: I am not a lawyer. 

Copyright rights on games is not as prohibitive as you may think (see
[this thread on BGG][12]).  What you _can_ copyright is original,
artistic expression.  That is, the copyrighted work must not be a copy
of something else (originality) and must be some form of expression.
One _cannot_ copyright ideas, only their expression in so far as it is
_artistic_ (i.e., not a trivial expression that anyone knowledgeable
within the field can do with rudimentary effort).

This means you _can not_ copyright your game mechanics, for example,
only how you described them.  You _can not_ copyright a title (but you
may be able to claim trademark on it).  You _can_ copyright the
wording of the rules, the graphics that you use, and so on.

This also means, that you are essentially free to make your own
version of previously published game, _as long as_ 

- you do not copy existing text 
- you do not copy existing graphics 
- you respect any kind of trademark claims

However, it is advisable to contact the copyright holders of the
previously published game to avoid [SNAFU][13].  If in doubt, seek
professional help. 

[12]: https://boardgamegeek.com/thread/493249/
[13]: https://en.wikipedia.org/wiki/SNAFU
