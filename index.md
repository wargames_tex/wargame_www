![](gamestack.png){style="float:right;padding-left:20px;max-width:300px;"}

# $\text{\sffamily\bfseries\LARGE\LaTeX}$ Print'n'Play Wargames 

This site has a number of revamp of older
[wargames](https://en.wikipedia.org/wiki/Wargame) (and a few related
types of games) as
[Print'n'Play](https://boardgamegeek.com/boardgamecategory/1120/print-play). 

The files are all PDFs and should be readable by any PDF viewer.  All
text and graphics in the games presented here, are entirely new -
nothing is copied from the originals.

[More, general, information](more.html){target=_self}

## License 

All materials provided on this site, unless explicitly noted
otherwise, are licensed under the Creative Commons
Attribution-ShareAlike 4.0 International License. To view a copy of
this license, visit
[http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/)
or send a letter to 

> Creative Commons
> PO Box 1866, Mountain View
> CA 94042
> USA
