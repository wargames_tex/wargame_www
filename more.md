# More information

## [How to use the PDFs](howto.html){target=_self}
## [VASSAL modules](vassal.html){target=_self}
## [BGG uploads](uploads.html){target=_self}
## [Sources](sources.html){target=_self}
## [Build the documents and modules](build.html){target=_self}
## [Feedback](feedback.html){target=_self}
## [Thoughts on &copy;](copyright.html){target=_self}
