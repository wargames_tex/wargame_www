# Sources 

All of the Print'n'Play versions of the games are produced using
[$\text{\large\textbf{\LaTeX}}$][latex] with the
[`wargame`][wargame_tex] package.  $\text{\LaTeX}$ ensures that text
is layed out in an aesthetically pleasing manner and following
time-tested typographic traditions.  [Tik&zwj;_z_][tikz] -
a powerful graphics package for $\text{\LaTeX}$ - and its use in the
[`wargame`][wargame_tex] package ensures high-quality, scale-able
graphics.  All this with the author focusing on content rather than
form.

The [`wargame`][wargame_tex] package likewise provides the opportunity
to create a draft [VASSAL][vassal] module. By supplying a
[Python][python] script that uses [`pywargame`][pywargame] API, a
final module can be made were complex behaviour is "programmed" in.

All sources for all [games](games.html) and [tools](tools.html) can be
found at GitLab at

- [https://gitlab.com/wargames_tex][wargames_tex]

The individual game and tool entries on this site links directly to
the game or tool's GitLab repositories.  At these repositories you will
also find the latest PDFs and VASSAL modules, ready for download. 

If you need some pointers as to how to build the documents and modules
from the sources, you can read [this page](build.html){target=_self}. 

[wargame_tex]: https://gitlab.com/wargames_tex/wargame_tex
[wargames_tex]: https://gitlab.com/wargames_tex
[pywargame]: https://gitlab.com/wargames_tex/pywargame
[vassal]: https://vassalengine.org
[latex]: https://latex-project.org
[tikz]: https://tikz.org
[python]: https://python.org
