# How to use the PDFs

* [What to get](#what-to-get){target=_self}
* [How I do it](#how-i-do-it){target=_self}
  * [Glue to cardboard ](#glue-to-cardboard){target=_self}
  * [Cut cardboard ](#cut-cardboard){target=_self}
  * [Boards](#boards){target=_self}
    * [Small boards](#small-boards){target=_self}
    * [Larger boards](#larger-boards){target=_self}
    * [Boards in general](#boards-in-general){target=_self}
  * [Counters](#counters){target=_self}
    * [Single sided counters ](#single-sided-counters){target=_self}
    * [Double sided counters ](#double-sided-counters){target=_self}
    * [Counters in general ](#counters-in-general){target=_self}
  * [Booklets](#booklets){target=_self}
  * [Charts](#charts){target=_self}
  * [Storage ](#storage){target=_self}

## What to get 

For each game you are interested in playing, download the needed
materials (typically the rules, board, and materials file).  Then you
need to mount counters and boards as you see fit.  There is a
[how-to](https://boardgamegeek.com/wiki/page/Print_and_Play_Games#)
page at BoardGameGeek that gives a number of tips on how to do that,
and below I explain [what I do](#how-i-do-it). 

Generally, the materials are available as 

- Full PDF for either A4 or Letter printers which includes
  _everything_, such as 
  - The rules 
  - Counters
  - Board (possibly split over several pages)
  - Orders of Battle charts 
  - and other materials 
  These PDFs are always designed to be printed on duplex
  (double-sided) printers.   This is also why, in the materials
  section, every even page is blank so as to now have anything on the
  backside of the materials that should be mounted. 
- Booklets of the rules in both A4 and Letter formats.  
  - Letter format does not reproduce the aspect ratio of a page when
    folded down the middle of the long side, unlike the A-formats
    which do (aspect ratio of &radic;2).  The Letter booklets
    therefore have additional white space on the top and bottom of the
    pages. 
- A single PDF with only the materials, such as counters, board,
  charts, and so on. 
- A single PDF with the board only, especially if the board is larger
  than an A4 or Letter page. 
  
You should download the version and format of the materials that best
suits your needs and available resources.  In most parts of the world
that means using the A4 PDFs (it's really
[only](https://en.wikipedia.org/wiki/Paper_size#/media/File:Prevalent_default_paper_size.svg)
North America that uses the Letter format), and for most users I would
recommend to take the Booklet and materials files and any possible
board file.

## How I do it

For glue, I typically use glue sticks.  I have access to an A3
printer, which means I can use that format for board prints. 

I use standard paper (75g/m2 roughly) for prints. 

### Glue to cardboard 

When I glue a print on to cardboard, I typically do the following. 

1. First, I align the print with an edge of the cardboard and make
   sure that it fits on the cardboard.
2. Then I lift a slip of the print at the edge I aligned, and hold
   down the rest of the print.  
3. I apply an even layer of glue on the cardboard
4. Then I lay down, _slowly_ the slip I lifted taking care to smooth
   out toward the edges. 
5. I lift up the rest of the print and again apply an even layer of
   glue to the rest of the cardboard.  Be careful to not wait too long
   after steps 3 and 4 to do this. 
6. Then I lay down the rest of the print very carefully and slowly.
   As I lay down the print, I smooth the print from the centre toward
   the edges. 
7. Once the entire print is glued to the cardboard, I try to smooth
   out any wrinkles or air bubbles using a folding knife 
8. Then I put the cardboard and glued on print between two heavy
   books, possibly large if the print is large. I then let it sit
   there for a good half hour before further processing. 

### Cut cardboard 

When I need to cut the cardboard, I use a metal ruler and a sharp,
pointy hobby knife.

1. I first lay down the ruler and align it to where I need to cut. 
2. Then I make a mark in the cardboard using the hobby knife - first
   in one end, then in the other.  
3. I then check the alignment of the ruler by placing the hobby knife
   in this small marks and press the ruler up against it. 
4. I then make a _shallow_ cut along the ruler, applying pressure to
   the ruler that it doesn't move. 
5. I make several _shallow_ cuts along the ruler, taking great care to
   not move the ruler.   If the ruler _does_ become misalign, I do the
   same thing as in point 3 above: Place the knife in the grove and
   press the ruler up against the knife, and do that at both ends. 

   _Do not_ cut too deep on each pass. That will require more pressure
   which is likely to misalign the ruler. 
   
   For 2mm cardboard, I typically need 4 to 6 passes to get all the
   way through everywhere along the cut. 
   
For groves, or folding lines, I typically only cut half-way through.

### Boards

#### Small boards

For smaller boards that either fits on an A4 or A3 page, I typically
glue a single sheet print of the board on to 2mm poster cardboard, and
then follow the procedure outlined [above](#cut-cardboard) to trim the
edges. 


I like to store my games in boxes designed to hold A4 paper, as it
integrates well on my shelves and because they are readily available.
However, if the board did not fit onto a single A4 sheet but rather a
single A3 sheet, then I have to make a folding grove.   To do that, I
measure the board along the long edge, and find the mid distance from
one edge. 

I make the grove on the _front_ side of the board (the side with the
print on it).  If I made the grove on the backside of the board, then
the print may fold poorly when the board is printed. 

I then follow the procedure [above](#cut-cardboard), _except_ I never
cut all the way through. 2 to 3 passes on 2mm cardboard is usually
enough.

I then carefully try to fold the board along the grove.  If it seems
to be too hard to fold, then I make 1 or 2 additional passes along the
grove, carefully checking each time if the grove is deep enough. 

#### Larger boards

For larger board, i.e., boards that do not fit on a single A3 sheet, I
typically use 1mm or 1 1/2mm poster carbon.  This is because such a
map need to be folded several times to fit in an A4 box, and the
folded board would otherwise quickly become very thick. 

How the board should be folded depends on the exact dimensions of the
board.  Some comments are given in the [`wargame`
manual](https://gitlab.com/wargames_tex/wargame_tex#a-package-to-make-hexncounter-wargames-in-latex). For
example a 2x4 fold board. 

![](board2x4.png)

For example, the Afrika Korp board is very long but not very wide.  So
I cut 6 folding groves along the length of the map, alternating
between the front and back side.   For the Afrika Korps map, it is
important that the last grove is on the back of the board.  

Then, I cut _all the way_ through, up to the last back-side grove.  I
can now fold each of the top and bottom halfs over the end.  Then I
can finally fold the last part of the map down to to firm the final
folded map. 

![](board2x7.png)

Different games have different board sizes, and it is therefore
important to plan ahead before creating groves and trimming.  A useful
way to visualise the final fold, is to make a small paper version of
the game (the aspect ratio is the important factor). 

This pattern can in principle be continued _ad infinitum_.  For
example for a 4x3 board, like for D-Day, one can cut through almost a
long the length of the board, alternating between starting at the left
and right side.    The exact pattern depends on how the board is best
divided into the desired folded size, but generally falls in the
following four cases 

**An odd number of rows or columns**

![](board3x3.png)

In the top, we can add as many rows as we want - the pattern stays the
same.  In the bottom case, we can add as many columns as we want
without changing the pattern. 

**An even number of rows or columns**

![](board4x3.png)

In the top, we can again add as many rows as we want - the pattern
stays the same.  In the bottom case, as before, we can add as many
columns as we want without changing the pattern.

I always want to chose the pattern that minimises the number of folds -
in particular folds on over the top of the board (grooves on the back
side), since they can mess with the glued-on print. 

#### Boards in general 

The board sheets does not have any
["bleed"](https://en.wikipedia.org/wiki/Bleed_(printing)) - extra
background around the desired area.   Typically, for off-set
printing, this "bleed" is there to ensure that the background fills
the print after trimming. However, for most modern printers, laserjet
or inkjet, this is not an issue.  Also, the board sheets are
constructed so that _at least_ 5mm around the edge of the paper sheet
is not printed upon, since most printers cannot print in those areas. 

### Counters 

#### Single sided counters 

I glue the counter sheet on to cardboard, as outlined
[above](#glue-to-cardboard).  I typically use 2mm poster cardboard for
this. 

#### Double sided counters 

1. I cut out the front of the counter sheet and glue that onto cardboard
   as outlined [above](#glue-to-cardboard). 
2. After the glue has dried, I then trim the front of the counters,
   glued to the cardboard, so that edges of the counter sheet are at
   the edges of the cardboard.
3. Then I cut out the back of the counter sheet. 
4. I [glue](#glue-to-cardboard) the back of the counter sheet to the
   back of cardboard with the front of the counters.  Here I take
   _extraordinary_ care to align the back sheet to the front.  
   * I typically hold the back counter sheet on to the cardboard and
     raise them up vertically align one edge, and
   * then _carefully_ lay them down and use the knife to adjust the
     sheet along the other edge. 
	 
![](countersglue.png)

Again, I typically use 2mm poster cardboard. 

An alternative way to produce double-sided counters, which I have had
less luck with, is to use thinner cardboard, say 1mm, and 

1. [Glue](#glue-to-cardboard) the full counter sheet, front _and_ back,
   on to the cardboard, and let the glue dry.  
2. Then [cut](#cut-cardboard) a grove on the _front_ of the cardboard
   along the line that separates the fronts and backs of the counters.
   This grove has to be _very_ precisely made, otherwise the front and
   back of the counters will be misaligned. 
3. Next, apply glue to either back side of the cardboard and then
   _carefully_ fold the to sides together and press them _hard_
   together.  You really have to be sure that the two cardboard pieces
   stick together _everywhere_. 
4. Place the now double-cardboard between two _very_ heavy books and
   let them dry. 

![](countersalt.png)

My problem with this method, is that the two sides of the card board
tend to separate.   The benefit of this method is that it is easier to
get good alignment between the front and back of the counters. 

I suggest you experiment and find the method that best suits you.
Perfect alignment between the front and back of counters is _very_
hard to get, and as long as they are not misaligned too badly, it is
not a big problem.   The biggest problem, in my mind, is if the
counters come "undone" (e.g., the print falls off or the counters
easily bends, or the like). 
   

#### Counters in general 

Once the glue as settled, I can cut out the counters. 

![](counters.png)

1. I cut first along the columns or row (which ever is longer) as
   described [above](#cut-cardboard), _except_ I do not cut all the
   way through - roughly 3/4 of the way through. 
2. Then I [cut](#cut-cardboard) along the rows or columns (the other
   direction of item 1), again _not_ all the way through but roughly
   3/4 of the way through.
3. Then I again cut along the same direction as in 1, and all the way
   through, _except_ for roughly half of the last counter.  I then
   have stripes of counters held together at the end. 
4. Finally, I cut all the way through along the direction of 2 above,
   separating out the individual counters, except for the last column.
   These I then cut out individually. 
   
The goal is to keep the counters together as long as possible to
ensure longer and more stable cuts.

The counter sheets does not have any
["bleed"](https://en.wikipedia.org/wiki/Bleed_(printing)) - extra
background colour around the counters.  Typically, for off-set
printing, this "bleed" is there to ensure that the background is
filled should the background become misaligned with the foreground, or
to ensure that the background fills the print after trimming.
However, for most modern printers, laserjet or inkjet, this is not an
issue.

### Booklets 

For the rules and other such materials, I typically take the
`*Booklet.pdf` version of the rules.

1. I then print this on a duplex (two-sided) printer.
2. Each sheet is then folded down the middle.  Here, I need to take
   care that I fold them the right direction so that the smaller page
   number is on the front.  I typically use a folding knife to make
   the fold sharp.
3. Then I put the folded sheets together in the proper order.
4. Using a proper stabler I stable, twice, the sheets together through
   the fold.  The stabler has to be able to reach the fold, meaning
   its lever arm should be at least 15cm long. 
   
### Charts 

For charts, such as Orders of Battle (OOBs), I typically
[glue](#glue-to-cardboard) the print onto sturdy (2mm or so) poster
cardboard.  This allows me to place counters on the OOBs and still
move them without the counters moving too much around.

For other charts, I do not typically mount them.  If the charts take
up a single page, I may print a copy of that page and glue it to the
back of the [storage](#storage) box.  If I do mount the charts, then I
will often use thinner cardboard, say 1mm thick or so. 

### Storage 

I typically use brown cardboard boxes (e.g., shirt boxes) dimensioned
to hold A4 paper to store the board, rules, sheets, charts, and the
smaller boxes of counters.  I typically also use smaller brown
cardboard boxes for the counters.  I typically store counters for each
faction, and general utility counters, in separate boxes.

I print the front page of the rule book and glue that on to the
lid of the box.  I make special images (`spine.tex` in the
repositories) for the sides of the box, and then glue them on to the
lid sides of the box.  I also make special images (`box.tex` in the
repositories) for the counter boxes.

If there are not that many counters, another storage option is to use
matchboxes.  For those I make special images (`box.tex`) that wrap
around the matchbox.
