# Games

Below are links to the [GitLab page](https://gitlab.com/wargames_tex)
page for each game. 

* [Afrika Korps](#afrika-korps){target=_self}
* [Battle](#battle){target=_self}
* [Battle for Moscow](#battle-for-moscow){target=_self}
* [D-Day (3rd Ed.)](#d-day-3rd-ed.){target=_self}
* [D-Day (Smithsonian)](#d-day-smithsonian){target=_self}
* [First Blood](#first-blood){target=_self}
* [Gettysburg (Smithsonian)](#gettysburg-smithsonian){target=_self}
* [Kriegspiel](#kriegspiel){target=_self}
* [Napoleon at Waterloo](#napoleon-at-waterloo){target=_self}
* [Outdoor Survival](#outdoor-survival){target=_self}
* [Panzerarmee Afrika](#panzerarmee-afrika){target=_self}
* [Port Stanley](#port-stanley){target=_self}
* [Strike Force One](#strike-force-one){target=_self}
* [The Drive on Metz](#the-drive-on-metz){target=_self}

At the bottom is a [summary](#summary){target=_self} of the games. 

## [![](https://gitlab.com/wargames_tex/afrikakorps_tex/-/raw/master/.imgs/front.png?ref_type=heads) Afrika Korps](https://gitlab.com/wargames_tex/afrikakorps_tex#afrika-korps) 

This simulates the North Africa campaign.  A relatively simple game
played on a large map.  One key to success in this game is to manage
logistics, while playing a placement game.   The game was originally
published in 1964 by the [Avalon Hill Game
Company](https://avalonhill.com). 
  
## [![](https://gitlab.com/wargames_tex/battle_tex/-/raw/master/.imgs/front.png?ref_type=heads) Battle](https://gitlab.com/wargames_tex/battle_tex#battle-the-game-of-generals)

An abstract game without die-rolls.  Terrain is placed on the map as
tiles and different set-up cards is used to simulate different periods
ranging from ancient to modern. The game was originally published by
Yaquinto Publications, Inc. in 1979.

## [![](https://gitlab.com/wargames_tex/bfm_tex/-/raw/master/.imgs/front.png?ref_type=heads) Battle for Moscow](https://gitlab.com/wargames_tex/bfm_tex#battle-for-moscow)

Introductory game on the German push towards Moscow in 1941.  This is
an excellent starting point for learning about wargames.  The game was
posted on [grognards.com](https://grognards.com/bfm) in 1996.

## [![](https://gitlab.com/wargames_tex/sbotb_tex/-/raw/master/.imgs/front.png?ref_type=heads) Battle of the Bulge (Smithsonian)](https://gitlab.com/wargames_tex/sbotb_tex#battle-of-the-bulge-day-smithsonian-edition)

Germany's attempt to push toward Antwerp and halt the Allied invasion
of Western Europe.  A relatively simple game (with more advanced
options) that can serve as an introduction to wargames.  The game was
originally published in 1991 by the [Avalon Hill Game
Company](https://avalonhill.com) in their _American History_ series
developed together with the [Smithsonian Institute](https://si.edu).

## [![](https://gitlab.com/wargames_tex/dday_tex/-/raw/master/.imgs/front.png?ref_type=heads) D-Day (3rd Ed.)](https://gitlab.com/wargames_tex/dday_tex#d-day)

From the invasion in Normandy to east of the Rhine.  Low to medium
complexity game that covers from the campaign from Normandy to the
final surrender.  The map is relatively large and there are many
counters.  The game was originally published in 1977 by the [Avalon
Hill Game Company](https://avalonhill.com).

## [![](https://gitlab.com/wargames_tex/sdday_tex/-/raw/master/.imgs/front.png?ref_type=heads) D-Day (Smithsonian)](https://gitlab.com/wargames_tex/sdday_tex#d-day-smithsonian-edition)

From the invasion in Normandy to east of the Rhine.  This is a simpler
version of the game above.  It is meant as an introduction to
wargames, but also has more advanced optional rules.  The game was
originally published in 1991 by the [Avalon Hill Game
Company](https://avalonhill.com) in their _American History_ series
developed together with the [Smithsonian Institute](https://si.edu).

## [![](https://gitlab.com/wargames_tex/firstblood_tex/-/raw/master/.imgs/front.png?ref_type=heads) First Blood](https://gitlab.com/wargames_tex/firstblood_tex#first-blood)

The first battle of the "Island hopping" campaign for the United
States of America to gain the upper hand in the Pacific.  A relatively
simple game which does include air and sea forces in an abstract
manner. The game was first published by [Avalon Hill International
Kriegspeil Society](https://ahiks.com) in 1991 in the _Kommandeur_
magazine.

## [![](https://gitlab.com/wargames_tex/sgb_tex/-/raw/master/.imgs/front.png?ref_type=heads) Gettysburg (Smithsonian)](https://gitlab.com/wargames_tex/sgb_tex#gettysburg-smithsonian-edition)

Lee's attempt at getting to Washington DC, via Pennsylvania.  A simple
game to play with a lot of challenges.  The game is meant as an
introductory game with more advanced optional rules.  The game was
originally published in 1988 by the [Avalon Hill Game
Company](https://avalonhill.com), and then again in 1991 as part of
their _American History_ series developed together with the
[Smithsonian Institute](https://si.edu).

## [![](https://gitlab.com/wargames_tex/kriegspiel_tex/-/raw/master/.imgs/front.png?ref_type=heads) Kriegspiel](https://gitlab.com/wargames_tex/kriegspiel_tex#kriegspiel)

An generic modern warfare wargame.  Red and Blue faction fight it out.
The game is relatively simple but has many optional rules to simulate
various aspects of modern warfare, including nuclear weapons.  The
game uses a dice-less combat resolution system where both sides select
offensive and defensive tactics.  The game was published in 1970 by
the [Avalon Hill Game Company](https://avalonhill.com).

## [![](https://gitlab.com/wargames_tex/naw_tex/-/raw/master/.imgs/front.png) Napoleon at Waterloo](https://gitlab.com/wargames_tex/naw_tex#napoleon-at-waterloo)

The famous battle between the Duke of Wellington and Field Marshall
Blücher on one side and Emperor Napoleon Bonaparte on the other.  The
battle spelled the end of Napoleon's second stint reign over France.
This introductory game was first published by [Simulations
Publications, Inc.](https://en.wikipedia.org/wiki/Simulations_Publications,_Inc.) in 1971. 

## [![](https://gitlab.com/wargames_tex/outdoor_tex/-/raw/master/.imgs/front.png?ref_type=heads) Outdoor Survival](https://gitlab.com/wargames_tex/outdoor_tex#outdoor-survival)

Survival in the wilderness.  Not a wargame, but has many of the
elements of a wargame (turns, hex'n'counter, die rolls).  Scenarios
provide and increasing complexity and difficulty.  Up to 6 persons can
play this game.  The game was originally published in 1972 by the [Avalon
Hill Game Company](https://avalonhill.com).

## [![](https://gitlab.com/wargames_tex/paa_tex/-/raw/master/.imgs/front.png?ref_type=heads) Panzerarmee Afrika](https://gitlab.com/wargames_tex/paa_tex#panzerarmee-afrika)

This simulates the North Africa campaign of WWII.  A medium simple
game played on a large map.  One key to success in this game is to
manage logistics, while playing a placement game.  The game was
originally published in 1973 by Simulations Publications Inc..

## [![](https://gitlab.com/wargames_tex/portstanley_tex/-/raw/master/.imgs/frontA4.png?ref_type=heads) Port Stanley](https://gitlab.com/wargames_tex/portstanley_tex#port-stanley)

The British campaign to retake the Falkland Islands after the
Argentine invasion in 1982.   This medium complexity game simulates
many elements of modern warfare: combined arms, logistics, special
operations, and so on.  The game is played on a relatively large map
but with a low counter density.  The game illustrates that the
conflict was not as sure a win for the British as it is often thought
to have been.  The game was published by World Wide Wargames (3W) in
1984 in the _The Wargamer_, magazine, issue 28.

## [![](https://gitlab.com/wargames_tex/sfo_tex/-/raw/main/.imgs/front.png?ref_type=heads) Strike Force One](https://gitlab.com/wargames_tex/sfo_tex#the-strike-force-one-wargame)

A simple, introductory game of a cold-war conflict in Germany, cirka
1980's.  The board is small and the number of units low.  The game can
be played at three different complexities: Basic, Advanced, and
Expert.  The Expert level introduces random events to provide
challenges for even the seasoned grognard.  This game was included in
Nicholas Palmer's 1977 book [The Comprehensive Guide to Board
Wargaming](https://www.vftt.co.uk/files/Comprehensive%20Guide%20to%20Board%20Wargaming.pdf). 

## [![](https://gitlab.com/wargames_tex/dom_tex/-/raw/master/.imgs/front.png?ref_type=heads) The Drive on Metz](https://gitlab.com/wargames_tex/dom_tex#the-drive-on-metz)

Patton's attempt to expedite the Allied push to the Rhine.  An
introductory game with more advanced optional rules.  The unit density
is not too low, and the map provide a number of challenges for both
factions.  The original game was published in James F. Dunnigan's [The
Complete Wargamer Handbook](https://archive.org/details/CompleteWargamesHandbookDunnigan/), in 1980.


## [Summary](games.html#summary){target=_self}

|**Game**|**Period**|**Theatre**|**Level**|**Hex scale**|**Unit scale**|**Turn scale**|**Turns**|**Unit density**|**Complexity**|**Solitaire**|
|-----|:---:|-----|-----|:---:|:---:|----:|----:|:---:|----:|----:|
| [Afrika Korps](#afrika-korps){target=_self} | WWII | Africa | Operational | 7.4km | &times; | 15 days | 38 | low | 2 | 7 |
| [Battle](#battle){target=_self} | Multi | - | Operational | - | &times;&times;&times; | - | - | medium | 2 | 6 |
| [Battle for Moscow](#battle-for-moscow){target=_self} | WWII | Eastern | Operational | 40km | &times;&times;&times; | 7 days | 7 | medium | 1 | 8 |
| [D-Day (3rd Ed.)](#d-day-3rd-ed.){target=_self} | WWII | Western | Operational | 25km | &times;&times; | 7 days | 52 | high | 2 | 7 |
| [D-Day (Smithsonian)](#d-day-smithsonian){target=_self} | WWII | Western | Operational | 37km | &times;&times;&times; | 1 month | 12 | high | 1 | 8 |
| [First Blood](#first blood){target=_self} | WWII | Pacific | Operational | 500m | \|\| | 10 days | 15 | medium | 2 | 8 |
| [Gettysburg (Smithsonian)](#gettysburg-smithsonian){target=_self} | Gunpowder | American civil war | Operational | 640m | &times;&times; | 2 hours | 24 | medium | 1 | 8 |
| [Kriegspiel](#kriegspiel){target=_self} | Modern | - | Operational | ? | &times;&times; | 1 month | ? | medium | 4 | 8 | 
| [Napoleon at Waterloo](#napoleon-at-waterloo){target=_self} | Napoleonic | Western | Operational | 400m | &times;&times; | 1 hour | 10 | medium | 1 | 9 |
| [Outdoor survival](#outdoor-survival){target=_self} | Modern | Wilderness | Tactical | 6km | Person | 1 day | - | low | 1 | 9 |
| [Panzerarmee Afrika](#panzerarmee-afrika){target=_self} | WWII | Africa | Operational | 19.2km | \|\|\| | 1 month | 20 | low | 3 | 8 |
| [Port Stanley](#port-stanley){target=_self} | Modern | Falkland | Operational | 2.8km | \| | 2 days | 15 | low | 8 | 3 |
| [Strike Force One](#strike-force-one){target=_self} | Modern | Europe | Operational | 1.6km | \| | 1 day | 4 | low | 1 | 8 |
| [The Drive on Metz](#the-drive-on-metz){target=_self} | WWII | Western | Operational | 4km | \|\|\| | 1 day | 7 | low | 1 | 9 |

| **Unit scale**               | **Name**  | **Modern # of soldiers** |
|:----------------------------:|:----------|:------------------------:|
| &bullet;                     | Squad     | 5-10                     |
| &bullet;&bullet;             | Section   | 7-13                     |
| &bullet;&bullet;&bullet;     | Platoon   | 25-40                    |
| \|                           | Company   | 60-250                   |
| \|\|                         | Battalion | 300-1 000                |
| \|\|\|                       | Regiment  | 500-2 000                |
| &times;                      | Brigade   | 2,000-5,000              |
| &times;&times;               | Division  | 10,000-20,000            |
| &times;&times;&times;        | Corps     | 30,000-60,000            |
| &times;&times;&times;&times; | Army      | 100,000                  |

+------------+-------------------------------------------------+
|            | Score                                           |
+            +----+----+----+----+----+----+----+----+----+----+
|            | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9  | 10 |
+:==========:+====+====+====+:==:+====+====+====+====+===:+====+
| Complexity | Low          |    Medium              |   High  |
+------------+----+----+----+----+----+----+----+----+----+----+
| Solitaire  | Unsuitable   |                        |Suitable |
+------------+----+----+----+----+----+----+----+----+----+----+



