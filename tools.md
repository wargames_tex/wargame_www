# Tools 

Below are links to the [GitLab](https://gitlab.com/wargames_tex) pages
for each of the tools I made.

## [![](wargame.png) L<span class="latex-a">A</span>T<span class="latex-e">E</span>X `wargame` package](https://gitlab.com/wargames_tex/wargame_tex#a-package-to-make-hexncounter-wargames-in-latex)

A package for
[$\text{\bfseries\large\LaTeX}$](https://latex-project.org) for
authoring hex'n'counter wargames.

All games presented on this site have been made with this package. 

A
[tutorial](https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/file/public/doc/latex/wargame/tutorial.pdf?job=dist)
is available. 

## [![](pywargame.png) Python `pywargame` module](https://gitlab.com/wargames_tex/pywargame#python-utilities-for-wargames)

A [Python](https://python.org) module to read and write content for
various boardgame software tools, such as
[VASSAL](https://vassalengine.org) and
[CyberBoard](https://cyberboard.org). 



